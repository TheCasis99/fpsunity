﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour {

	[SerializeField]
	public Stats health;

	public int killCount;
	public int enemies;

	public int[] zoneEnemies;

	private void Awake(){
		health.Initialize ();
	}

	void Update () {
		if(health.CurrentVal <= 0f){
            
            SceneManager.LoadScene ("GameOver");

		}

		if (killCount == enemies) {
			Invoke ("Win", 3f);
		}

	}
		
	void Win(){
		SceneManager.LoadScene ("Win");
	}
}
