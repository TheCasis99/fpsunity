﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{

    public GameObject Menu;
    public GameObject Options;

	private void Awake(){
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
	}

    public void PlayGame()
    {
        SceneManager.LoadScene("game");
    }

    public void Option()
    {
        SceneManager.LoadScene("MainMenu");
    }

    
}
