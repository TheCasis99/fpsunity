﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {

	[SerializeField]
	private Stats healthStat;

	private Animator anim;
	private CapsuleCollider col;
	public float lookRadius = 10f;
	public PlayerBehaviour playerHealth;

	public AudioClip[] audio;
	private AudioSource aSource;

	Transform target;
	NavMeshAgent agent;

	private void Awake(){
		healthStat.Initialize ();
	}

	public void Start(){
		aSource = GetComponent<AudioSource>();
		col = GetComponent<CapsuleCollider> ();
		target = PlayerManager.intance.Player.transform;
		agent = GetComponent<NavMeshAgent> ();
		anim = GetComponent<Animator> ();
	}

	public void ApplyDamage(float damage)
	{
		healthStat.CurrentVal -= damage;
		aSource.clip = audio[0];
		aSource.Play ();
		if (healthStat.CurrentVal <= 0f)
		{
			Death ();
		}
	}

	public void Update () {
		
		float distance = Vector3.Distance (target.position, transform.position);

		if (distance <= lookRadius) {
			agent.SetDestination (target.position);
			Walk ();

			if (distance <= agent.stoppingDistance) {
				Attack ();
				FaceTarget ();
			}
		} else {
			agent.SetDestination (transform.position);
			Idle ();
		}
	}

	void FaceTarget(){
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation (new Vector3(direction.x, 0, direction.z));
		transform.rotation = Quaternion.Slerp (transform.rotation, lookRotation, Time.deltaTime * 5f);
	}

	void OnDrawGizmosSelected(){
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, lookRadius);
	}

	void Attack(){
		playerHealth.health.CurrentVal -= 0.1f;
		anim.SetBool ("walk", false);
		anim.SetBool ("attack", true);
	}

	void Idle(){
		anim.SetBool ("walk", false);
		anim.SetBool ("attack", false);
	}

	void Walk(){
		anim.SetBool ("walk", true);
		anim.SetBool ("attack", false);
	}

	void Death(){
		playerHealth.killCount++;
		Debug.Log (playerHealth.killCount);
		aSource.clip = audio[1];
		aSource.Play ();
		col.enabled = false;
		anim.SetBool ("death", true);
		lookRadius = 0;
	}
}
