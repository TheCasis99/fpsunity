﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public GameObject Menu;
    public GameObject Options;

	void Awake(){
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
	}

    public void PlayGame()
    {
        SceneManager.LoadScene("game");
    }

    public void Option()
    {
        Menu.SetActive(false);
        Options.SetActive(true);
    }

    public void Back()
    {
        Menu.SetActive(true);
        Options.SetActive(false);
    }

	public void Quit(){
		Application.Quit ();
	}
}
