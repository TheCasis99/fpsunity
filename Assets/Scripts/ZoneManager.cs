﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneManager : MonoBehaviour {

	[SerializeField]
	private PlayerBehaviour player;

	[SerializeField]
	private GameObject zone1;

	[SerializeField]
	private GameObject zone2;

	[SerializeField]
	private GameObject zone3;

	[SerializeField]
	private GameObject zone4;


	void Awake(){
		zone1.SetActive (true);
		zone2.SetActive (false);
		zone3.SetActive (false);
		zone4.SetActive (false);
	}

	void Update(){
		
		if(player.killCount == player.zoneEnemies[0]){
			Invoke ("Zone2", 3f);
		}
		if(player.killCount == player.zoneEnemies[1]+player.zoneEnemies[0]){
			Invoke ("Zone3", 3f);
		}
		if(player.killCount == player.zoneEnemies[1]+player.zoneEnemies[0]+player.zoneEnemies[2]){
			Invoke ("Zone4", 3f);
		}

	}

	void Zone2(){
		zone2.SetActive (true);
		zone1.SetActive (false);
	}
	void Zone3(){
		zone3.SetActive (true);
		zone2.SetActive (false);
	}
	void Zone4(){
		zone4.SetActive (true);
		zone3.SetActive (false);
	}

}
