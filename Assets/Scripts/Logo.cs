﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Logo : MonoBehaviour {

	private float cont = 4f;

	void Awake(){
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.None;
	}

	void Update(){

		cont -= Time.deltaTime;
		if(cont<=0){
			SceneManager.LoadScene ("MainMenu");
		}
	}

}
