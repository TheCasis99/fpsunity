﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	#region Singleton

	public static PlayerManager intance;

	void Awake(){
		intance = this;
	}

	#endregion

	public GameObject Player;

}
