﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    private Animator anim;

    private AudioSource _AudioSource;

    public float range = 100f;
    public int bulletsPerMag = 30;
    public int bulletsLeft = 200;

    public int currentBullets;


   
    public Transform shootPoint;
    public GameObject hitParticles;
    public GameObject bulletImpact;



    public ParticleSystem muzzleFlash;
    public AudioClip shootSound;



    public float fireRate = 0.1f;
    public float damage = 20f;



    float fireTimer;

    private bool isReloading;

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
        _AudioSource = GetComponent<AudioSource>();

        currentBullets = bulletsPerMag;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetButton("Fire1"))
        {
            if (currentBullets > 0)
                Fire();
            else if (bulletsLeft > 0)
                doReload();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if(currentBullets< bulletsPerMag && bulletsLeft > 0)
            doReload();
        }
            

        if (fireTimer < fireRate)
            fireTimer += Time.deltaTime;
                                    
	}

    void FixedUpdate()
    {
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);

        if (info.IsName("Fire")) anim.SetBool("Fire", false);

        isReloading = info.IsName("Reload");
    }

    private void Fire()
    {
        if (fireTimer < fireRate || currentBullets <=0 || isReloading )
            return;
        

        RaycastHit hit;

        if (Physics.Raycast(shootPoint.position, shootPoint.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name + "found!");
            GameObject hitParticleEffect = Instantiate(hitParticles, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
            hitParticleEffect.transform.SetParent(hit.transform);

            GameObject bulletHole = Instantiate(bulletImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));

            Destroy(hitParticleEffect, 1f);
            Destroy(bulletHole, 1.5f);

            if(hit.transform.GetComponent<HealthController>())
            {
                hit.transform.GetComponent<HealthController>().ApplyDamage(damage);
            }

			if(hit.transform.GetComponent<EnemyController>())
			{
				hit.transform.GetComponent<EnemyController>().ApplyDamage(damage);
			}

        }

        anim.CrossFadeInFixedTime ("Fire", 0.1f);
        muzzleFlash.Play();
        PlayShootSound();

        anim.SetBool("Fire", true);
        currentBullets--;
        fireTimer = 0.0f;

    }
    public void Reload()
    {
        if (bulletsLeft <= 0) return;

        int bulletsToLoad = bulletsPerMag - currentBullets;
        int bulletsToDeduct = (bulletsLeft >= bulletsToLoad) ? bulletsToLoad : bulletsLeft;

        bulletsLeft -= bulletsToDeduct;
        currentBullets += bulletsToDeduct;
    }

    private void doReload()
    {
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);

        if (isReloading) return;
        anim.CrossFadeInFixedTime("Reload",0.01f);
    }

    private void PlayShootSound()
    {
        _AudioSource.PlayOneShot(shootSound);
        
    }
}
